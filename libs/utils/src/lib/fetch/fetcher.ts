/**
 * Utils - API - Fetcher
 */

const headerMaker = (data: any) => {
  if (Object.keys(data).length) {
    const header = new Headers();
    Object.keys(data).forEach((item: string) => {
      header.append(item, data[item]);
    });

    return header;
  } else {
    return {};
  }
};

export const fetcher = ({
  baseUrl,
  params = {},
  route,
  headers,
  ...rest
}: any) => {
  let url = `${baseUrl}${route}`;
  if (params) {
    url += new URLSearchParams({ ...params });
  }

  const data = {
    headers: headerMaker({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers,
    }),
    method: 'GET',
    ...rest,
  };

  return fetch(url, data);
};
