/**
 * Libs - Checkbox
 */

import { ICheckbox } from './types/checkboxProps';
import React from 'react';

export const Checkbox = ({
  defaultChecked,
  checked,
  name,
  onSubmit,
}: ICheckbox): JSX.Element => {
  const handleCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { checked: status } = e.target;

    return onSubmit && onSubmit({ status });
  };
  return (
    <label className="relative inline-flex items-center cursor-pointer">
      <input
        defaultChecked={defaultChecked}
        checked={checked}
        className="sr-only peer"
        name={name}
        id={name}
        type="checkbox"
        onChange={handleCheckboxChange}
      />

      <div className="w-11 h-6 bg-gray-500 rounded-full dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white-0 after:border-white-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue"></div>
    </label>
  );
};
