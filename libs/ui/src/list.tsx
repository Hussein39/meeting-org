/**
 * Ui - List
 */

import { ListProps } from './types/props';

export const List = ({ children, title, subTitle }: ListProps) => (
  <div className="bg-white h-[65%]  md:h-[66%] xl:h-[69%] rounded-lg shadow-lg border-2 border-gray-600 dark:border-none">
    <h2 className="flex bg-white-200 border-b-2 border-gray-600 rounded-tl-lg rounded-tr-lg justify-between px-2 text-gl font-semibold py-2 dark:bg-darkGray-200 dark:border-none">
      <div className="text-black dark:text-white-0">{title}</div>
      <div className="dark:text-white-0">{subTitle}</div>
    </h2>
    <div className="h-[89%] md:h-[84%] xl:h-[88%] overflow-y-auto dark:bg-darkBlack rounded-bl-lg rounded-br-lg">
      {children}
    </div>
  </div>
);
