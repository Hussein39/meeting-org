/**
 * Ui - Item
 */

import { ItemProps } from './types/props';

export function Item({ children, item, key }: ItemProps) {
  return (
    <div
      className={
        'flex justify-between py-2 md:py-2 border-b-2 dark:border-b-darkGray-300'
      }
      key={key}
    >
      <div className="px-2 md:8 dark:text-white-0">{item.name}</div>
      <div className="px-2 md:8">{children}</div>
    </div>
  );
}
