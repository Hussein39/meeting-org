/**
 * Types - Dropdown Types
 */

export interface IDropdown {
  label:string;
  name: string;
  options:IOption[];
  onSubmit: (e: any) => void
}

export interface IOption {
  name: string;
  value: string;
}
