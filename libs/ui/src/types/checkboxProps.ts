/**
 * Types - Checkbox Props
 */

export interface ICheckbox {
  defaultChecked?: boolean,
  checked?: boolean
  name: string
  onSubmit: ({ status } :{ status: boolean }) => void
}
