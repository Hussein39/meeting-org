/**
 * Types
 */

export interface Value {
  name: string;
  status: number;
}

export interface ListProps {
  children: JSX.Element[] | JSX.Element;
  title: string;
  subTitle: string;
}

export interface ItemProps {
  children: JSX.Element;
  item: Value;
  key: string;
}
