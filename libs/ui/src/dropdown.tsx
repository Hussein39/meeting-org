/**
 * Ui - Dropdown
 */

import React from 'react';
import { IDropdown, IOption } from './types/dropdownProps';

export const Dropdown = ({ label, name, options, onSubmit }: IDropdown) => {
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const { value } = e.target;
    onSubmit({ value });
  };

  return (
    <div className="relative">
      <div className="fle w-full md:w-64 items-center justify-between py-2 rounded- text-left">
        <div className="relative inline-block w-full md:w-64">
          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-darkGray-100">
            {label}
          </label>

          <select
            id={name}
            onChange={handleChange}
            className="bg-white-0 border border-gray-500 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:bg-darkBlack dark:text-white-0 dark:border-none"
          >
            {options.map((item: IOption) => (
              <option key={item.value} value={item.value}>
                {item.name}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
};
