/**
 * App - Component - Dark Mode Switcher
 */

import { Checkbox } from '@meeting-org/ui';
import { useEffect } from 'react';

const DarkModeSwitcher = () => {
  const modeStatus =
    localStorage.getItem('lightMode') === 'true' ? true : false;

  useEffect(() => {
    mode(modeStatus);
  }, []);

  const mode = (status: boolean) => {
    const root = window.document.documentElement;
    root.classList.toggle('dark', status);
  };

  const handleSubmit = ({ status }: { status: boolean }) => {
    mode(status);
    localStorage.setItem('lightMode', status.toString());
  };

  return (
    <Checkbox
      defaultChecked={modeStatus}
      name="darkMode"
      onSubmit={handleSubmit}
    />
  );
};

export default DarkModeSwitcher;
