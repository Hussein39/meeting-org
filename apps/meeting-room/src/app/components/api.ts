/**
 * App - Component - API
 */

import { fetcher } from '@meeting-org/utils';

export const getHotel = async () => {
  const response = await fetcher({
    baseUrl: process.env.NX_API_URL,
    route: '/hotels',
  });

  return response.json();
};

export const getChannel = async (id: string) => {
  const response = await fetcher({
    baseUrl: process.env.NX_API_URL,
    route: '/channel/' + id,
  });
  return response.json();
};

export const updateChannel = async (id: string, status: boolean) => {
  const response = await fetcher({
    baseUrl: process.env.NX_API_URL,
    route: '/channel/' + id,
    method: 'PUT',
    body: JSON.stringify({ status: status ? 1 : 0 }),
  });
  return response.json();
};
