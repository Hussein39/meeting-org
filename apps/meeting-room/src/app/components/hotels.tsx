/**
 * App - Component - Hotels
 */

import { Dropdown } from '@meeting-org/ui';

interface Hotel {
  name: string;
  id: string;
}

interface HotelProps {
  hotels: Hotel[];
  onSubmit: ({ value }: { value: string }) => void;
}

const Hotels = ({ hotels, onSubmit }: HotelProps) => {
  const options = hotels.map(({ id: value, name }: Hotel) => ({
    name,
    value,
  }));

  return (
    <Dropdown
      name="dropdown"
      label="Hotel"
      onSubmit={onSubmit}
      options={options}
    />
  );
};

export default Hotels;
