/**
 * Component - Header
 */

import DarkModeSwitcher from './darkModeSwitcher';

const Header = () => {
  return (
    <header className="flex bg-white-0 justify-between items-center px-4 py-2 text-xl font-bold border-white-100 rounded-tl-lg rounded-tr-lg border-b border-b-2 flex-none dark:bg-darkBlack dark:border-b-black">
      <h1 className="text-black text-blue dark:text-white-0 flex">
        <img
          className="pr-2 h-4 mt-2"
          src="/assets/images/logo.png"
          alt="logo"
        />
        Meeging Package
      </h1>
      <div>
        <DarkModeSwitcher />
      </div>
    </header>
  );
};
export default Header;
