/**
 * App - Component - Channel
 */

import { Checkbox, Item, List } from '@meeting-org/ui';
import Loading from './loading';

interface IChannelItem {
  name: string;
  hotel_id: number;
  status: number;
  id: number;
}

interface IChannelProps {
  channels: IChannelItem[];
  loading: boolean;
  onSubmit: ({ status }: { identify: string; status: boolean }) => void;
}

const Channels = ({ channels, loading, onSubmit }: IChannelProps) => {
  return (
    <div className="h-full px-4 relative">
      <Loading isLoading={loading} />
      <List title="Cahnnel" subTitle="Visibility">
        {channels.map(
          (listItem: IChannelItem, index: number): JSX.Element => (
            <Item item={listItem} key={`item#${index}`}>
              <Checkbox
                name={listItem.name + listItem.hotel_id}
                checked={listItem.status > 0 ? true : false}
                onSubmit={(data) => {
                  return onSubmit({
                    ...data,
                    identify: listItem.id.toString(),
                  });
                }}
              />
            </Item>
          )
        )}
      </List>
    </div>
  );
};

export default Channels;
