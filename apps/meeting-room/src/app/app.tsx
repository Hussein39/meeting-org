/**
 * Src - App
 */

// React
import { useState, useEffect } from 'react';

// API
import { getHotel, getChannel, updateChannel } from './components/api';

// Components
import Channels from './components/channels';
import Hotels from './components/hotels';
import Header from './components/header';

function App() {
  const [loading, setLoading] = useState(false);
  const [hotels, setHotels] = useState([]);
  const [channels, setChannels] = useState([]);
  const [currentHotel, setCurrentHotel] = useState('1');

  useEffect(() => {
    setLoading(true);

    getHotel().then(({ data }) => {
      setHotels(data);

      getChannel(currentHotel).then(({ rows }) => {
        setChannels(rows);
        setLoading(false);
      });
    });
  }, []);

  const onSubmit = (data: { identify: string; status: boolean }) => {
    setLoading(true);

    updateChannel(data.identify, data.status).then(() => {
      getChannel(currentHotel).then(({ rows }) => {
        setChannels(rows);
        setLoading(false);
      });
    });
  };

  const handleHotelSubmit = ({ value }: { value: string }) => {
    setLoading(true);
    setCurrentHotel(value);

    getChannel(value).then(({ rows }) => {
      setChannels(rows);
      setLoading(false);
    });
  };

  return (
    <div className="md:flex md:items-center md:justify-center h-screen w-screen bg-white-0 dark:bg-gray-400">
      <div className="h-full md:h-[95%] xl:h-[95%] bg-white-0 border-2 border-white-100 md:w-11/12 lg:w-5/6 xl:w-3/6 rounded-md shadow-lg dark:bg-darkBG dark:border-none">
        <Header />

        <div className="flex-none py-4 px-4">
          <header className="text-xl font-bold dark:text-darkGray-100">
            Channel manager
          </header>

          <Hotels hotels={hotels} onSubmit={handleHotelSubmit} />
        </div>
        <Channels loading={loading} channels={channels} onSubmit={onSubmit} />
      </div>
    </div>
  );
}

export default App;
