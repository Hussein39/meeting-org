const { createGlobPatternsForDependencies } = require('@nx/react/tailwind');
const { join } = require('path');

module.exports = {
  darkMode: 'class',
  content: [
    join(
      __dirname,
      '{src,pages,components,app}/**/*!(*.stories|*.spec).{ts,tsx,html}'
    ),
    ...createGlobPatternsForDependencies(__dirname),
  ],
  theme: {
    extend: {
      colors: {
        white: {
          0: '#ffffff',
          100: "#f0f0f1",
          200: '#f8fafc',
          300: "#f0f3f8",
        },
        black: '#111111',
        blue: '#0050FF',
        gray: {
          100:  '#5e5f61',
          400: "#8290a2",
          500: "#cbd5e1",
          600: '#e5e5e5',
        },
        //dark mode
        darkBlack:"#202938",
        darkBG:'#121828',
        darkGray: {
          100: '#949aa8',
          200:'#384152',
          300: '#2d3645',
        }
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
