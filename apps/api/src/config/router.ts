/**
 * App - Router
 */

import express from 'express';

// Hotel
import getHotels from '../app/controller/hotel.controller'

// Channel
import { getChannel, updateChannel } from '../app/controller/channel.controler';

const router = express.Router();

/*
 * Channel Entity
 */
router.put('/channel/:id',  updateChannel)
router.get('/channel/:id', getChannel)

/**
 * Hotel Entity
 */
router.get('/hotels', getHotels)


export default router
