/**
 * Controller - Hotel Controller
 */

import getAllHotels from "../model/hotel.model"

 const getHotels = async (_, res) => {
  try {
    const hotels = await getAllHotels();
    res.json({ data: hotels });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: e.message });
  }
};

export default getHotels
