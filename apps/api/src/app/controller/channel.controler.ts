/**
 * Controller - Channel Controller
 */

import { getChannelsByHotelId, updateChannelStatus } from '../model/channel.mode';

export const getChannel = async (req, res) => {
  try {
    const { id } = req.params;
    const rows = await getChannelsByHotelId(id);
    res.json({ rows });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: e.message });
  }
};

export const updateChannel= async (req, res) => {
  try {
    const { status } = req.body;
    const { id } = req.params;

    await updateChannelStatus(id, status);
    res.json({ message: 'Channel updated successfully' });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: e.message });
  }
};
