/**
 * Controller - App Controller
 */

import { initializeDatabase } from '../model/db.model';

const port = process.env.PORT || 3333;

export const setupApp = (app) => {
  initializeDatabase()
    .then(() => {
      app.listen(port, () => {
        console.log(`Server is running on http://localhost:${port}`);
      });
    })
    .catch((error) => {
      console.error('Error initializing database:', error);
    });
};
