/**
 * App - Model - Channels
 */

import sqlite3 from 'sqlite3';
import { dbPath } from '../../config/constant'

export const getChannelsByHotelId = (hotelId) => {
  return new Promise((resolve, reject) => {
    const db = new sqlite3.Database(dbPath);

    db.all('SELECT * from channels WHERE hotel_id = ?', [hotelId], (err, rows) => {
      db.close();
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
};

export const updateChannelStatus = (id, status) => {
  return new Promise((resolve, reject) => {
    const db = new sqlite3.Database(dbPath);
    db.run('UPDATE channels SET status = ? WHERE id = ?', [status, id], (err) => {
      db.close();
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
};
