/**
 * App - Model - Hotel
 */

import sqlite3 from 'sqlite3';
import { dbPath } from '../../config/constant';

const getAllHotels = () => {
  return new Promise((resolve, reject) => {
    const db = new sqlite3.Database(dbPath);
    db.all('SELECT * FROM hotel', (err, rows) => {
      db.close();
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
};

export default  getAllHotels

