/**
 * App - Model - DB
 */

import createHotelTable from '../helpers/createHotelTable'
import createChannelsTable from '../helpers/createChannelsTable';
import createDefaultChannels from '../helpers/createDefaultChannels';
import createDefaultHotels from '../helpers/createDefaultHotels'
import tableExists from '../helpers/tableExists'


export const initializeDatabase = () => {
  return new Promise<void>((resolve, reject) => {
    const setupDatabase = async () => {
      try {
        const isHotelExists = await tableExists('hotel')

        if(!isHotelExists) {
          console.info('Init DB ...')
          console.info('Creating Hotel Table ...')
          await createHotelTable();
          console.info('Hotel Table is created.')
          console.info('Adding initial data to Hotel table.')
          await createDefaultHotels();
        }

        const isChannelExists = await tableExists('channels')
        if(!isChannelExists) {
          console.info('Creating Channel Table ...')
          await createChannelsTable();
          console.info('Channel Table is created.')
          console.info('Adding initial data to Channel table.')
          await createDefaultChannels();
          console.info("DONE")
        }

        resolve();
      } catch (error) {
        reject(error);
      }
    };
    setupDatabase()

  });
};
