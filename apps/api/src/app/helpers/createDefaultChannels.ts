/**
 * App - Helpers - Create Default Table
 */

import sqlite3 from 'sqlite3';
import { dbPath } from '../../config/constant';

const createDefaultChannels = () => {
  const db = new sqlite3.Database(dbPath);
  return new Promise<void>((resolve, reject) => {
    db.get('SELECT COUNT(*) AS count FROM channels', (err, row: {count: number}) => {
      if (err) {
        db.close()
        reject(err);
      } else {
        if (row.count === 0) {
          const defaultChannels = [];
          for (let hotelId = 1; hotelId <= 5; hotelId++) {
            for (let channel = 1; channel <= 10; channel++) {
              defaultChannels.push({
                name: `Channel ${channel}`,
                status: 0,
                hotel_id: hotelId,
              });
            }
          }
          const stmt = db.prepare(
            'INSERT INTO channels (name, status, hotel_id) VALUES (?, ?, ?)'
          );
          defaultChannels.forEach((channel) => {
            stmt.run(channel.name, channel.status, channel.hotel_id);
          });

          stmt.finalize((err) => {
            if (err) {
              db.close()
              reject(err);
            } else {
              db.close()
              resolve();
            }
          });
        } else {
          db.close()
          resolve();
        }
      }
    });
  });
};

export default createDefaultChannels
