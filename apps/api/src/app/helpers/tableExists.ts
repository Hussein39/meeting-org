
/**
 * App - Helpers - Table Exists
 */

import sqlite3 from 'sqlite3';
import { dbPath } from '../../config/constant';

const tableExists = (tableName: string): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    const db = new sqlite3.Database(dbPath);

    // Query to check if the table exists in the database
    db.get(
      `SELECT name FROM sqlite_master WHERE type='table' AND name=?`,
      [tableName],
      (err, row) => {
        if (err) {
          reject(err);
        } else {
          resolve(!!row); // Return true if row exists (table exists), otherwise false
        }
        db.close();
      }
    );
  });
};

export default tableExists
