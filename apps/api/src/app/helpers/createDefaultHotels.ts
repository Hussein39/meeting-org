
/**
 * App - Helpers - Create Default Hotels Table
 */

import sqlite3 from 'sqlite3';
import { dbPath } from '../../config/constant';

const createDefaultHotels = () => {
  const db = new sqlite3.Database(dbPath);
  return new Promise<void>((resolve, reject) => {
    db.get('SELECT COUNT(*) AS count FROM hotel', (err, row: { count: number }) => {
      if (err) {
        db.close()
        reject(err);
      } else {
        if (row.count === 0) {
          const defaultHotels = [
            { name: 'Hotel A' },
            { name: 'Hotel B' },
            { name: 'Hotel C' },
            { name: 'Hotel D' },
            { name: 'Hotel E' },
          ];

          const stmt = db.prepare('INSERT INTO hotel (name) VALUES (?)');
          defaultHotels.forEach((hotel) => {
            stmt.run(hotel.name);
          });

          stmt.finalize((err) => {
            if (err) {
              reject(err);
            } else {
              resolve();
            }
            db.close()
          });
        } else {
          db.close()
          resolve();
        }
      }
    });
  });
};

export default createDefaultHotels
