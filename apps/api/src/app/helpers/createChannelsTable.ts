/**
 * App - Helpers - Create Channels Table
 */

import sqlite3 from 'sqlite3';
import { dbPath } from '../../config/constant';

const createChannelsTable = () => {
  const db = new sqlite3.Database(dbPath);
  return new Promise<void>((resolve, reject) => {
    db.exec(`
      CREATE TABLE IF NOT EXISTS channels (
        id INTEGER PRIMARY KEY,
        name TEXT,
        status INTEGER,
        hotel_id INTEGER,
        FOREIGN KEY (hotel_id) REFERENCES hotel(id)
      )
    `, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
      db.close()
    });
  })
}

export default createChannelsTable
