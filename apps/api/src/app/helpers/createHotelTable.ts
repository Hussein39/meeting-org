
/**
 * App - Helpers - Create Hotels Table
 */

import sqlite3 from 'sqlite3';
import { dbPath } from '../../config/constant';


const createHotelTable = () => {
  const db = new sqlite3.Database(dbPath);

  return new Promise<void>((resolve, reject) => {
    db.exec(
      `
      CREATE TABLE IF NOT EXISTS hotel (
        id INTEGER PRIMARY KEY,
        name TEXT
      )
    `,
      (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
        db.close()
      }
    );

  });
};

export default createHotelTable
