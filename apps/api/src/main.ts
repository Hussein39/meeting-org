/**
 * App
 */

import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

import router from './config/router';
import { setupApp } from './app/controller/appController';

const app = express();

app.use(express.json());
app.use(morgan('dev'))
app.use(cors());
app.use(router);


setupApp(app)
