# MeetingOrg

Document on local setup, starting and extending NX. 

**Node:** v18.16.0 

**Npm:** 9.7.1

# Serve Apps

## Move .env
```
mv .env.example .env
```
## Install package

```
npm i
```

## start server

```
npm run start:server
```

## start client

```
npm run start:web
```
